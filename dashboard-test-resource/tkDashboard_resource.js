(function () {
  angular
    .module('tkDashboard')
    .factory('Dashboard', Dashboard)
    .factory('Widget', Widget)
    .factory('WidgetType', WidgetType);

  function Dashboard($resource){
    return $resource('api/dashboard/:id');
  }

  function Widget($resource){
    return $resource('api/widget/:id');
  }

  function WidgetType($resource){
    return $resource('api/widget-type/:id');
  }
})();