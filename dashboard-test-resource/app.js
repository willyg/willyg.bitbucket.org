(function () {
  angular
    .module('myApp',['ngRoute', 'tkDashboard'])
    .config(config);

  function config($routeProvider){
    $routeProvider
      .when('/', {
        templateUrl: 'tkDashboard_template_list.html',
        controller: 'DashboardListController'
      })
      .when('/dashboard/:dashboardId', {
        templateUrl: 'tkDashboard_template.html',
        controller: 'DashboardController'
      });
  }
})();