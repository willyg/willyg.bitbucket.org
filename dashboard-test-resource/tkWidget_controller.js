(function () {
  angular
    .module('tkDashboard')
    .controller('WidgetController', WidgetController)
    .controller('ModalInstanceController', ModalInstanceController);

  function WidgetController($scope, Widget, WidgetType, $modal, $rootScope) {
    $scope.widget = null;
    $scope.isDeleteable = true;
    $scope.isConfigurable = true;
    $scope.content = null;

    $scope.reload = reload;
    $scope.edit = edit;

    Widget.get({id: $scope.widgetId}, function (widget) {
      $scope.widget = widget;

      WidgetType.get({id: widget.widget_template_id}, function(widget_template){
        $scope.isDeleteable = widget_template.isDeleteable;
        $scope.isConfigurable = widget_template.isConfigurable;
        $scope.content = widget_template.template;
        if (!$scope.widget.configs)
          $scope.widget.configs = widget_template.configs;
      });
    });

    function reload() {
      $rootScope.$broadcast('widget_' + $scope.widget.id + '_reload');
    }

    function edit() {
      var modalInstance = $modal.open({
        templateUrl: 'tkWidgetConfig.html',
        controller: ModalInstanceController,
        resolve:{
          title: function () {
            return $scope.widget.title;
          },
          size: function () {
            return $scope.widget.size;
          },
          configs: function(){
            return $scope.widget.configs;
          }
        }
      });
      modalInstance.result.then(function(widget){
        $scope.widget.title = widget.title;
        $scope.widget.size = widget.size;
        $scope.widget.configs = widget.configs;
        reload();
      })
    }
  }


  function ModalInstanceController($scope, $modalInstance, title, size, configs){
    $scope.widget = {title: title, size: size, configs: configs};
    $scope.cancel = cancel;
    $scope.save = save;

    function cancel(){
      $modalInstance.dismiss('cancel');
    }

    function save(){
      $modalInstance.close($scope.widget);
    }
  }
})();