(function () {
  angular
    .module('tkDashboard')
    .controller('DashboardController', DashboardController)
    .controller('DashboardListController', DashboardListController);

  function DashboardController($scope, Dashboard, $routeParams, WidgetType, $modal)
  {
    $scope.remove = remove;
    $scope.addWidget = addWidget;

    Dashboard.get({id: $routeParams.dashboardId}, function (data) {
      $scope.dashboard = data;
    });

    function remove(idx){
      $scope.dashboard.widgets.splice(idx, 1);
    }

    function addWidget(){
      var widgetTypes = [];

      WidgetType.query(function(items){
        var modalInstance = $modal.open({
          templateUrl: 'addWidget.html',
          controller: AddWidgetController,
          resolve: {
            widgetTypes: function(){
              return items;
            }
          }
        });

        modalInstance.result.then(function(selectedWidgetType){
          alert('Must deploy server first');
          console.log(selectedWidgetType);
          //Should be replace with POST to server
//          var widget = {
//            id: Math.floor((Math.random() * 1000) + 1),
//            title: selectedWidgetType.name,
//            size: 4,
//            widget_template_id: selectedWidgetType.id
//          };
//
//          WidgetType.get({id: selectedWidgetType.id}, function(data){
//            widget.configs = data.configs;
//            $scope.dashboard.widgets.push(widget);
//            console.log($scope.dashboard.widgets);
//          });

        });
      });
    }
  }

  function AddWidgetController($scope, $modalInstance, widgetTypes){
    $scope.widgetTypes = widgetTypes;
    $scope.selected = {
      widgetType: widgetTypes[1]
    };
    $scope.save = save;
    $scope.cancel = cancel;

    function save(){
      $modalInstance.close($scope.selected.widgetType);
    }

    function cancel(){
      $modalInstance.dismiss('cancel');
    }
  }

  function DashboardListController($scope, Dashboard){

    Dashboard.query(function(dashboards){
      $scope.dashboards = [];
      angular.forEach(dashboards, function(dashboard){
        $scope.dashboards.push(dashboard);
      });
    });
  }
})();