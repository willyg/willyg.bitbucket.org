(function () {
  angular
    .module('tkDashboard')
    .directive('tkWidget', tkWidget);

  function tkWidget() {
    return{
      restrict: 'EA',
      templateUrl: 'tkWidget_template.html',
      controller: 'WidgetController',
      transclude: true,
      scope:{
        widgetId: '='
      }
    }
  }
})();