(function () {
  angular
    .module('tkDashboard')
    .controller('CatWidgetController', CatWidgetController);

  function CatWidgetController($scope) {
    var url = 'http://thecatapi.com/api/images/get?format=src&type=png';
    $scope.image_url = null;
    $scope.getImage = getImage;
    $scope.isLoaded = true;

    //Reload action
    $scope.$on('widget_' + $scope.widget.id + '_reload', function(){
      getImage();
    });

    function getImage(){
      $scope.isLoaded = false;
      $scope.image_url = url + '&ts=' + Math.floor((Math.random() * 1000) + 1);

      var image = new Image();
      image.onload = function(){
        $scope.$apply(function(){
          $scope.isLoaded = true;
        });
      };

      image.src = $scope.image_url;
    }
  }
})();