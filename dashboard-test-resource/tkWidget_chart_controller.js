(function(){
  angular
    .module('tkDashboard')
    .controller('ChartWidgetController', ChartWidgetController);

  function ChartWidgetController($scope) {
    var chart1 = {};
    chart1.type = "ColumnChart";
    chart1.data = {"cols": [
      {id: "month", label: "Month", type: "string"},
      {id: "laptop-id", label: "Laptop", type: "number"},
      {id: "desktop-id", label: "Desktop", type: "number"},
      {id: "server-id", label: "Server", type: "number"},
      {id: "cost-id", label: "Shipping", type: "number"}
    ], "rows": [
      {c: [
        {v: "January"},
        {v: 19, f: "42 items"},
        {v: 12, f: "Ony 12 items"},
        {v: 7, f: "7 servers"},
        {v: 4}
      ]},
      {c: [
        {v: "February"},
        {v: 13},
        {v: 1, f: "1 unit (Out of stock this month)"},
        {v: 12},
        {v: 2}
      ]},
      {c: [
        {v: "March"},
        {v: 24},
        {v: 0},
        {v: 11},
        {v: 6}

      ]}
    ]};

    chart1.options = {
      "title": "Sales per month",
      "isStacked": "true",
      "fill": 20,
      "displayExactValues": true,
      "vAxis": {
        "title": "Sales unit", "gridlines": {"count": 6}
      },
      "hAxis": {
        "title": "Date"
      },
      height: 500
    };

    chart1.formatters = {};

    $scope.chart = chart1;


    //Reload action
    $scope.$on('widget_' + $scope.widget.id + '_reload', function(){
      angular.forEach($scope.widget.configs, function(config){
        if (config.name == 'type')
          $scope.chart.type = config.value;
        if (config.name == 'height')
          $scope.chart.options.height = config.value;
      });
    });
  }
})();