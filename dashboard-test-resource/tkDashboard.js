(function () {
  angular
    .module('tkDashboard', ['ngResource', 'ngSanitize', 'ngTouch', 'ui.bootstrap','ui.sortable', 'googlechart']);
})();