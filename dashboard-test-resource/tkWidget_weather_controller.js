(function () {
  angular
    .module('tkDashboard')
    .controller('WeatherWidgetController', WeatherWidgetController);

  function WeatherWidgetController($scope, $http){
    $scope.name = 'This is weather';
    $scope.city = null;
    $scope.currentTemp = null;
    $scope.icon = '';
    $scope.isLoaded = false;
    $scope.getData = getData;

    var iconUrl = 'http://openweathermap.org/img/w/';

    //Reload action
    $scope.$on('widget_' + $scope.widget.id + '_reload', function(){
      getData();
    });

    function loadCity(){
      angular.forEach($scope.widget.configs, function(config){
        if (config.name == 'city')
          $scope.city = config.value;
      });
    }

    function getData(){
      loadCity();
      $scope.isLoaded = false;
      $http({
        method: 'JSONP',
        url: 'http://api.openweathermap.org/data/2.5/weather',
        params: {
          q: $scope.city,
          callback: 'JSON_CALLBACK'
        }
      }).success(function(data){
        $scope.isLoaded = true;
        $scope.currentTemp = data.main.temp - 273.15;
        $scope.icon = iconUrl + data.weather[0].icon + '.png';
      });
    }
  }
})();