(function () {
  angular
    .module('myApp')
    .directive('widget', widget);

  function widget(){
    return{
      restrict: 'A',
      templateUrl: 'widget/widget.html',
      controller: 'WidgetController',
      scope: {
        widgetId: '=',
        dashboardId: '='
      }
    }
  }
})();