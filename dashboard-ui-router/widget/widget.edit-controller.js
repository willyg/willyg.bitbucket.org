(function () {
  angular
    .module('myApp')
    .controller('WidgetEditController', WidgetEditController);

  function WidgetEditController($scope, Widget){
    $scope.save = save;
    $scope.backup = null;
    $scope.cancel = cancel;

    init();

    function save(){
      $scope.$parent.$parent.isEdit = false;
      $scope.$parent.$parent.reload();
      Widget.update($scope.widget.id, $scope.widget.title, $scope.widget.size, $scope.widget.widgetTypeId, $scope.widget.configs);
    }

    function init(){
      $scope.widget = Widget.get($scope.widget.id);
      $scope.backup = angular.copy($scope.widget);
    }

    function cancel(){
      angular.copy($scope.backup, $scope.widget);
      $scope.$parent.$parent.isEdit = false;
    }
  }
})();