(function () {
  angular
    .module('myApp')
    .factory('Widget', Widget);

  function Widget($filter){
    var _widgets_stubs = [
      {id: 1001, title: "Sydney's Weather", size: 6, widgetTypeId: 9991, configs: {city: 'Sydney', something: 'Blahblah'}},
      {id: 1002, title: "Melbourne's Weather", size: 6, widgetTypeId: 9991, configs: {city: 'Melbourne'}},
      {id: 1003, title: "Cat Picture", size: 4, widgetTypeId: 9992}
    ];

    var _widgets = angular.fromJson(localStorage.widgets) || _widgets_stubs;

    return{
      getAll: getAll,
      get: get,
      remove: remove,
      insert: insert,
      update: update,
      addNew: addNew,
    };

    function getAll(){
      return _widgets
    }

    function get(id){
      return $filter('filter')(_widgets, {id: id})[0];
    }

    function remove(id){
      console.log(_widgets);
      for (var i = 0; i < _widgets.length; i++){
        if (_widgets[i].id == id){
          _widgets.splice(i, 1);
        }
      }
      console.log(_widgets);
      save();
    }

    function insert(widget, insertAfterId){
      var id = insertAfterId || _widgets[_widgets.length - 1].id;
      for (var i = 0; i < _widgets.length; i++){
        if (_widgets[i].id == id){
          _widgets.splice(i, 0, widget);
        }
      }
      save();
    }

    function update(id, title, size, widgetTypeId, configs){
      for (var i = 0; i < _widgets.length; i++){
        if (_widgets[i].id == id){
          _widgets[i].title = title;
          _widgets[i].size = size;
          _widgets[i].widgetTypeId = widgetTypeId;
          _widgets[i].configs = configs;
        }
      }
      save();
    }

    function addNew(widgetType){
      var widgetId = _widgets[_widgets.length - 1].id + 1;
      var newWidget = {id: widgetId, title: "", size: 4, widgetTypeId: widgetType.id, configs: widgetType.configs};
      _widgets.push(newWidget);
      save();
      return widgetId;
    }

    function save(){
      localStorage.widgets = angular.toJson(_widgets);
    }
  }
})();