(function () {
  angular
    .module('myApp')
    .controller('WidgetController', WidgetController);

  function WidgetController($rootScope, $scope, Widget, Dashboard, WidgetType){
    $scope.widget = null;
    $scope.isDeleteable = true;
    $scope.isConfigurable = true;
    $scope.template = null;
    $scope.isEdit = false;

    $scope.reload = reload;
    $scope.edit = edit;
    $scope.remove = remove;

    init();

    function reload() {
      $rootScope.$broadcast('widget_' + $scope.widget.id + '_reload');
    }

    function edit(){
      $scope.isEdit = true;
    }

    function remove(dashboardId, widgetId) {
      Dashboard.removeWidget(dashboardId, widgetId);
    }

    function init(){
      $scope.widget = Widget.get($scope.widgetId);
      var widgetType = WidgetType.get($scope.widget.widgetTypeId);
      $scope.isDeleteable = widgetType.isDeleteable;
      $scope.isConfigurable = widgetType.isConfigurable;
      $scope.template = widgetType.template;
    }

  }
})();