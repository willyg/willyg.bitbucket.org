(function(){
  angular
    .module('myApp', ['ui.router', 'ui.bootstrap'])
    .config(config);

  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/dashboard/10');
  }
})();