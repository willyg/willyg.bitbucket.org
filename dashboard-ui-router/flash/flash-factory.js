(function () {
  angular
    .module('myApp')
    .factory('flash', flash);

  function flash(){
    var messages = [];

    return{
      setMessage: setMessage,
      getMessage: getMessage
    };

    function setMessage(message){
      messages.push(message);
    }

    function getMessage(){
      return messages.shift() || "";
    }
  }
})();