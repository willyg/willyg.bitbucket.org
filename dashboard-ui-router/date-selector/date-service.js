(function () {
  angular
    .module('myApp')
    .factory('dateService', dateService);

  function dateService($filter){
    var relations = [
      {symbol: '-', label: 'before'},
      {symbol: '+', label: 'after'}
    ];

    var timeUnits = [
      {symbol: 'd', label: 'days'},
      {symbol: 'w', label: 'weeks'},
      {symbol: 'm', label: 'months'},
      {symbol: 'y', label: 'years'}
    ];

    var isPopupOpen = false;

    return{
      parseRelativeDate: parseRelativeDate,
      relations: relations,
      timeUnits: timeUnits,
      toString: toString,
      isAbsoluteDate: isAbsoluteDate,
      getPopupOpen: getPopupOpen,
      setPopupOpen: setPopupOpen
    };

    function parseRelativeDate(relativeDateString){
      var relativeDateRegex = /([-|\+]*)([0-9]+)(d|w|m|y)/;
      var relativeObject = {
        relation: null,
        duration: null,
        timeUnit: null
      };

      String(relativeDateString).replace(relativeDateRegex, function ($0, $1, $2, $3){
        relativeObject.relation = $filter('filter')(relations, {symbol: $1 || '+'})[0];
        relativeObject.duration = $2;
        relativeObject.timeUnit = $filter('filter')(timeUnits, {symbol: $3})[0];
      });

      return relativeObject;
    }

    function toString(relation, duration, timeUnit){
      return duration + " " + timeUnit.label + " " + relation.label + " today";
    }

    function isAbsoluteDate(dateString){
      var dateObject = new Date(dateString);
      return dateObject != "Invalid Date";
    }

    function getPopupOpen(){
      return isPopupOpen;
    }

    function setPopupOpen(status){
      isPopupOpen = status;
    }
  }
})();