(function () {
  angular
    .module('myApp')
    .directive('dateSelectorPopup', dateSelectorPopup);

  function dateSelectorPopup(){
    return{
      restrict: 'A',
      templateUrl: 'date-selector/date-selector-popup.html',
      scope: {
        dateModel: '='
      }
    }
  }
})();