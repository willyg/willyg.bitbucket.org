(function () {
  angular
    .module('myApp')
    .directive('dateSelector', dateSelector);

  function dateSelector(){
    return{
      restrict: 'A',
      templateUrl: 'date-selector/date-selector.html',
      controller: 'DateSelectorController',
      scope: {
        dateModel: '=',
        isPopupOpen: '='
      },
      link: function(scope, element, attrs){
        element.bind('click', function(event){
          event.stopPropagation();
        })
      }
    }
  }
})();