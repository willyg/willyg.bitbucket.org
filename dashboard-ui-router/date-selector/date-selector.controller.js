(function () {
  angular
    .module('myApp')
    .controller('DateSelectorController', DateSelectorController);

  function DateSelectorController($scope, dateService){
    $scope.open = open;
    $scope.updateRelative = updateRelative;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.relativeDate = null;
    $scope.absoluteDate = null;
    $scope.selectedDuration = null;
    $scope.selectedRelation = null;
    $scope.selectedTimeUnit = null;
    $scope.format = 'dd-MMMM-yyyy';
    $scope.relations = dateService.relations;
    $scope.timeUnits = dateService.timeUnits;

    init();

    function init(){
      $scope.tempDateModel = $scope.dateModel;
      if (dateService.isAbsoluteDate($scope.tempDateModel)){
        $scope.isRelativeDate = false;
        $scope.absoluteDate = new Date($scope.tempDateModel);
      }else{
        $scope.isRelativeDate = true;
        $scope.relativeDate = $scope.tempDateModel;
        parseRelativeDate();
      }

      $scope.$watch('absoluteDate', function (newValue){
        if (newValue)
          $scope.tempDateModel = newValue;
      });

      $scope.$watch('relativeDate', function (newValue){
        if (newValue)
          $scope.tempDateModel = newValue;
      });
    }

    function parseRelativeDate(){
      var relativeObject = dateService.parseRelativeDate($scope.relativeDate);
      $scope.selectedRelation = relativeObject.relation;
      $scope.selectedDuration = relativeObject.duration;
      $scope.selectedTimeUnit = relativeObject.timeUnit;
    }

    function open($event){
      event.preventDefault();
      event.stopPropagation();

      $scope.isOpened = true
    }

    function updateRelative(){
      if ($scope.selectedDuration && $scope.selectedRelation && $scope.selectedTimeUnit)
        $scope.relativeDate = $scope.selectedRelation.symbol + $scope.selectedDuration + $scope.selectedTimeUnit.symbol;
    }

    function save(){
      $scope.dateModel = $scope.tempDateModel;
      cancel();
    }

    function cancel(){
      $scope.isPopupOpen = false;
    }
  }
})();