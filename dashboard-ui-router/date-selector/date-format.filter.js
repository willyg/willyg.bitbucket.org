(function () {
  angular
    .module('myApp')
    .filter('dateFormat', dateFormat);

  function dateFormat($filter, dateService){
    return function(dateString, format){
      if (dateService.isAbsoluteDate(dateString)){
        return $filter('date')(new Date(dateString), format);
      }

      var relativeObject = dateService.parseRelativeDate(dateString);
      return dateService.toString(
        relativeObject.relation,
        relativeObject.duration,
        relativeObject.timeUnit
      );
    }
  }

})();