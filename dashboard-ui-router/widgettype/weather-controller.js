(function () {
  angular
    .module('myApp')
    .controller('WeatherWidgetController', WeatherWidgetController);

  function WeatherWidgetController($scope, $http){
    $scope.name = 'This is weather';
    $scope.city = null;
    $scope.currentTemp = null;
    $scope.icon = '';
    $scope.isLoaded = false;
    $scope.reload = reload;

    var iconUrl = 'http://openweathermap.org/img/w/';

    //Reload action
    $scope.$on('widget_' + $scope.widget.id + '_reload', function(){
      reload();
    });

    function reload(){
      $scope.city = $scope.widget.configs.city;
      $scope.isLoaded = false;

      $http({
        method: 'JSONP',
        url: 'http://api.openweathermap.org/data/2.5/weather',
        params: {
          q: $scope.city,
          callback: 'JSON_CALLBACK'
        }
      }).success(function(data){
        $scope.isLoaded = true;
        $scope.currentTemp = data.main.temp - 273.15;
        $scope.icon = iconUrl + data.weather[0].icon + '.png';
      });
    }
  }
})();