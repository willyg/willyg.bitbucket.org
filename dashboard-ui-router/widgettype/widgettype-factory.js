(function () {
  angular
    .module('myApp')
    .factory('WidgetType', WidgetType);

  function WidgetType($filter){
    var _widgetTypes= [
      {id: 9991, template: "widgettype/weather.html", isDeleteable: true, isConfigurable: true, configs: {city: "Melbourne"}},
      {id: 9992, template: "widgettype/cat.html", isDeleteable: true, isConfigurable: true, configs: {}}
    ];

    return{
      getAll: getAll,
      get: get
    };

    function getAll(){
      return _widgetTypes;
    }

    function get(id){
      return $filter('filter')(_widgetTypes, {id: id})[0];
    }

  }
})();