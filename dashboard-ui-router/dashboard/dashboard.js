(function () {
  angular
    .module('myApp')
    .config(config);

  function config($stateProvider){
    $stateProvider
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'dashboard/dashboard.html',
        controller: 'DashboardController'
      })
      .state('dashboard.detail', {
        url: '/:dashboardId',
        views: {
          '': {
            templateUrl: 'dashboard/dashboard.detail.html',
            controller: 'DashboardDetailController'
          },
          'dashboardName': {
            template: '{{dashboard.name}}',
            controller: 'DashboardDetailController'
          },
          'addWidget':{
            templateUrl: 'dashboard/widget.select.html',
            controller: 'DashboardDetailController'
          }
        }

      });
  }
})();