(function () {
  angular
    .module('myApp')
    .factory('Dashboard', Dashboard);

  function Dashboard($filter){
    var _dashboards_stubs = [
      {id: 10, name: 'Dashboard a', widgets: [1001, 1002]},
      {id: 44, name: 'Dashboard q', widgets: []},
      {id: 11, name: 'Cat\'s Dashboard', widgets: [1003]},
      {id: 43, name: 'Dashboard r', widgets: []},
      {id: 51, name: 'Dashboard t', widgets: []}
    ];

    var _dashboards = angular.fromJson(localStorage.dashboards) || _dashboards_stubs;

    return {
      getDashboards: getDashboards,
      getDashboard: getDashboard,
      insertDashboard: insertDashboard,
      removeDashboard: removeDashboard,
      removeWidget: removeWidget,
      addWidget: addWidget
    };

    function getDashboards(){
      return _dashboards;
    }

    function getDashboard(dashboardId){
      return $filter('filter')(_dashboards, {id: dashboardId})[0];
    }

    function insertDashboard(dashboard){
      _dashboards.push(dashboard);
    }

    function removeDashboard(dashboardId){
      for (var i = 0; i < _dashboards.length; i++){
        if (_dashboards[i].id == dashboardId){
          _dashboards.splice(i, 1);
        }
      }
    }

    function removeWidget(dashboardId, widgetId){
      for (var i = 0; i < _dashboards.length; i++){
        if (_dashboards[i].id == dashboardId){
          for (var j = 0; j < _dashboards[i].widgets.length; j++){
            if (_dashboards[i].widgets[j] == widgetId){
              _dashboards[i].widgets.splice(j, 1);
            }
          }
        }
      }
      save();
    }

    function addWidget(dashboardId, widgetId) {
      for (var i = 0; i < _dashboards.length; i++) {
        if (_dashboards[i].id == dashboardId) {
          _dashboards[i].widgets.push(widgetId);
        }
      }
      save();
    }

    function save(){
      localStorage.dashboards = angular.toJson(_dashboards);
    }
  }

})();