(function () {
  angular
    .module('myApp')
    .controller('DashboardController', DashboardController)
    .controller('DashboardDetailController', DashboardDetailController);

  function DashboardController($scope, Dashboard){
    $scope.dashboards = Dashboard.getDashboards();
  }

  function DashboardDetailController($scope, $stateParams, Dashboard, $state, WidgetType, Widget){
    $scope.dashboard = Dashboard.getDashboard($stateParams.dashboardId);
    $scope.remove = remove;
    $scope.isAddWidget = false;
    $scope.toggleAddWidget = toggleAddWidget;
    $scope.widgetTypes = WidgetType.getAll();
    $scope.selectedWidgetType = $scope.widgetTypes[0];
    $scope.addWidget = addWidget;

    function remove(dashboardId){
      Dashboard.removeDashboard(dashboardId);
      $state.go('dashboard.detail', {dashboardId: Dashboard.getDashboard().id});
    }

    function toggleAddWidget(){
      $scope.isAddWidget = !$scope.isAddWidget;
    }

    function addWidget(){
      toggleAddWidget();
      var widgetId = Widget.addNew($scope.selectedWidgetType);
      Dashboard.addWidget($scope.dashboard.id, widgetId);
    }
  }
})();